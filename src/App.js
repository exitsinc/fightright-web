import React from 'react'
import './App.css'
import './Components/Dummy/dummy.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
// import dummy from './Components/Dummy/dummy';
import Navbar from './Components/navbar/Navbar'
import Sidebar from './Components/Sidebar/Sidebar'
// import Order from './Components/Order/order';
import SwitchComp from './Components/SwitchComp'
import Calendar from './Components/Calendar/DemoApp'

export default function App() {
  return (
    <Router>
      <div style={{ height: '100vh' }}>
        <Navbar />
        <div style={{ display: 'flex' }}>
          <Sidebar />

          <div
            style={{
              display: 'flex',
              flex: 0.9,
              padding: '2rem 2rem',
              flexDirection: 'column',
              backgroundColor: '#f9f9f9',
            }}
          >
            {/* <MiddleSection /> */}
            <SwitchComp />
            {/* <Calendar /> */}
          </div>
        </div>
      </div>
    </Router>
  )
}

// const dump = () => {
//   return(
//       <>
//         <Navbar />
//         <Sidebar />
//         <Switch>
//           {/* <Route exact path="/dummy" component={dummy} />  */}
//           <Order />
//         </Switch>
//       </>
//   )
// }
