import React from "react";
import Table from "../Table/Table";
import "./order.css";
import Tbody from "../Table/Tbody/Tbody";
import { BsThreeDots } from "react-icons/bs";
import Pagination from "react-bootstrap/Pagination";

export default function order() {
  return (
    <>
      <div className="container-fluid" style={{  }}>
        <div className="row">
          <div className="col-lg-12" style={{ backgroundColor: "#f7f7f7" }}>
            <div
              style={{
                background: "#f7f7f7",
                padding: "1rem 2rem",
              }}
            >
              <h3 style={{ marginBottom: 10 }}>Case Diary</h3>
              <div
                style={{
                  background: "#fff",
                  padding: 15,
                  border: ".2rem solid #ececec",
                  borderRadius: "8px",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <div>
                    <h6 style={{ color: "#808080", fontWeight: "bold" }}>
                      Cases(1)
                    </h6>
                  </div>
                  <div>
                    <BsThreeDots fontSize={24} fontWeight="bold" style={{color:"#858585"}} />
                  </div>
                </div>
                <div>
                  <div
                    className="row"
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      padding: "1.0rem 1.5rem",
                    }}
                  >
                    <table
                      class="table"
                      className="borderless"
                      id="RoundedTable"
                    >
                      <thead>
                        <tr>
                          <th
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Date
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Title
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                              flexDirection: "column",
                            }}
                          >
                            Appare Model
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Petitioner
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Case No.
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Priorty
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Category
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Before Judge
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Classification
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                              textAlign: "center",
                            }}
                          >
                            Status
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem",
                              fontSize: "12px",
                            }}
                          >
                            Action
                          </th>
                        </tr>
                      </thead>

                      <div style={{ marginTop: 20 }}></div>

                      <Table
                        date="15/02/2020"
                        title="Enter the title"
                        Petitioner=""
                        Petitioner2="Petitioner"
                        caseno="3"
                        Critical="Super Critical"
                        Select="Selection"
                        Judge="Before Hon`ble Judge"
                        Classification="Classification"
                        bgcolor="#5df888"
                        status="Completed"
                        // dot="..."
                      />
                      <div style={{ marginTop: 20 }}></div>

                      <Table
                        date="15/02/2020"
                        title="Enter the title"
                        Petitioner=""
                        Petitioner2="Petitioner"
                        caseno="3"
                        Critical="Super Critical"
                        Select="Selection"
                        Judge="Before Hon`ble Judge"
                        Classification="Classification"
                        bgcolor="#ec1616"
                        status="Pending"
                        // dot="..."
                      />
                      <div style={{ marginTop: 20 }}></div>

                      <Table
                        date="15/02/2020"
                        title="Enter the title"
                        Petitioner=""
                        Petitioner2="Petitioner"
                        caseno="3"
                        Critical="Super Critical"
                        Select="Selection"
                        Judge="Before Hon`ble Judge"
                        Classification="Classification"
                        bgcolor="#ec1616"
                        status="Pending"
                        // dot="..."
                      />
                      <div style={{ marginTop: 20 }}></div>

                      <Table
                        date="15/02/2020"
                        title="Enter the title"
                        Petitioner=""
                        Petitioner2="Petitioner"
                        caseno="3"
                        Critical="Super Critical"
                        Select="Selection"
                        Judge="Before Hon`ble Judge"
                        Classification="Classification"
                        bgcolor="#5df888"
                        status="Completed"
                        // dot="..."
                      />
                    </table>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        paddingTop: "1rem",
                      }}
                    >
                      <Pagination>
                        <Pagination.Prev />
                        <Pagination.Item>{1}</Pagination.Item>
                        <Pagination.Item>{2}</Pagination.Item>
                        <Pagination.Item>{3}</Pagination.Item>
                        <Pagination.Item>{4}</Pagination.Item>
                        <Pagination.Item>{5}</Pagination.Item>
                        <Pagination.Item>{6}</Pagination.Item>
                        <Pagination.Item>{7}</Pagination.Item>
                        <Pagination.Next />
                      </Pagination>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              style={{
                background: "#f7f7f7",
                // height: "100vh",
                padding: "1rem 2rem",
              }}
            >
              <h3 style={{ marginBottom: 10 }}>To-Do's</h3>
              <div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: "1.25rem",
                  }}
                >
                  <div>
                    <h6 style={{ color: "#808080", fontWeight: "bold", color:"#858585" }}>
                      To-Do`s(0)
                    </h6>
                  </div>
                  <div>
                    <BsThreeDots fontSize={24} fontWeight="bold" />
                  </div>
                </div>
              </div>
              <div
                style={{
                  background: "#fff",
                  padding: 15,
                  border: ".2rem solid #ececec",
                  borderRadius: "8px",
                }}
              >
                <div>
                  <div
                    className="row"
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      padding: "1.0rem 1.5rem",
                    }}
                  >
                    <table class="table" className="borderless" id="todos">
                      <thead>
                        <tr>
                          <th
                            style={{
                              borderBottom: "none",
                              padding: "1rem 2rem ",
                              fontSize: "12px",
                            }}
                          >
                            From Date
                          </th>

                          <th
                            style={{
                              borderBottom: "none",
                              padding: "1rem 2rem",
                              fontSize: "12px",
                              flexDirection: "column",
                            }}
                          >
                            To Date
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem 5rem",
                              fontSize: "12px",
                            }}
                          >
                            Description
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              paddingLeft: "3rem",
                              fontSize: "12px",
                            }}
                          >
                            Related To
                          </th>

                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem 3rem",
                              fontSize: "12px",
                              textAlign: "center",
                            }}
                          >
                            Status
                          </th>
                          <th
                            scope="col"
                            style={{
                              borderBottom: "none",
                              padding: "1rem 3rem",
                              fontSize: "12px",
                            }}
                          >
                            Action
                          </th>
                        </tr>
                      </thead>
                      <Tbody
                        fromdate="21/06/2021"
                        todate="06/06/2020"
                        Description="DescriptionOne"
                        Related="Related To"
                        Pending="Completed"
                        bgcolor="#5df888"
                        // dot="..."
                      />
                      <div style={{ marginTop: 20 }}></div>
                      <Tbody
                        fromdate="21/06/2021"
                        todate="06/06/2020"
                        Description="DescriptionOne"
                        Related="Related To"
                        Pending="Pending"
                        bgcolor="#ec1616"
                        // dot="..."
                      />
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
