import React from "react";
import "./dashboard.css";
import { GrNotification } from "react-icons/gr";
import { CgProfile } from "react-icons/cg";
import { AiOutlineAppstore } from "react-icons/ai";
import { BiCalendar } from "react-icons/bi";


export default function Dashboard() {
  return (
    <>
      <div
        className="header"
        style={{
          display: "flex",
          justifyContent: "space-between",
          flexDirection: "row",
        }}
      >
        <div>
          <img className="header-logo" src="img/logo.png" />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            flexDirection: "row",
          }}
        >
          <div style={{ padding: "0 2rem" }}>
            <GrNotification size={32} />
          </div>
          <div
            style={{ padding: "0 2rem", display: "flex", flexDirection: "row" }}
          >
            <CgProfile size={32} /> <p style={{ margin: "0 1rem" }}>ANDREW</p>
          </div>
        </div>
      </div>
      <div
        style={{
          width: "300px",
          backgroundColor: "#e2e2e3",
          height: "100vh",
          padding: "3rem 0rem",
        }}
      >
        <div
          style={{
            backgroundColor: "#09b2f0",
            borderTopRightRadius: "50px",
            borderBottomRightRadius: "50px",
            marginRight: "1rem",
            display: "flex",
            flexDirection: "row",
          }}
        >
          <div
            style={{
              paddingLeft: "2rem",
              paddingTop: "0.3rem",
              paddingBottom: "0.2rem",
            }}
          >
            <AiOutlineAppstore
              style={{ color: "white", height: "40px", width: "40px" }}
            />
          </div>
          <div
            style={{ marginTop: "14px", marginLeft: "1rem", color: "white" }}>
            <h4>Dashboard</h4>
          </div>
          
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            margin:'1rem 0'
          }}
        >
          <div
            style={{
              paddingLeft: "2rem",
              paddingTop: "0.3rem",
              paddingBottom: "0.2rem",
            }}
          >
            <BiCalendar
              style={{ color: "#09b2f0", height: "40px", width: "40px" }}
            />
          </div>
          <div
            style={{ marginTop: "-5px", marginLeft: "1rem", color: "#858585" }}>
            <h4>Calendar</h4>
          </div>
        </div>
      </div>
    </>
  );
}
