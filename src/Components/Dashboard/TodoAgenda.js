import React from "react";

export default function TodoAgenda({ agendaData }) {
  return (
    <>
      {agendaData.map((d, i) => {
        return (
          <div
            style={{
              height: 70,
              width: 70,
              flexDirection: "column",
              justifyContent: "space-between",
              padding: 8,
              borderRadius: 5,
              marginRight: 15,
              backgroundColor: d.bgColor,
            }}
          >
            <p style={{ color: "white", fontSize: 12 }}>{d.title} </p>
            <p style={{ color: "white", fontWeight: "bold", fontSize: 12 }}>
              {d.time}
            </p>
          </div>
        );
      })}
    </>
  );
}
