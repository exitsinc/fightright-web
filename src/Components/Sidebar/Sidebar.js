import React from 'react'
import '../Dummy/dummy.css'
import { GrNotification } from 'react-icons/gr'
import { CgProfile } from 'react-icons/cg'
import { AiOutlineAppstore, AiOutlineSound } from 'react-icons/ai'
import { BiCalendar, BiSave, BiBarChartSquare } from 'react-icons/bi'
import { IoIosArrowDropright } from 'react-icons/io'
import { HiOutlineDocumentText, HiOutlineTicket } from 'react-icons/hi'
import { BsBriefcase, BsThreeDots } from 'react-icons/bs'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import { BsDownload } from 'react-icons/bs'
import { FiFilter } from 'react-icons/fi'
import { HiOutlineSearch } from 'react-icons/hi'
import { Link } from 'react-router-dom'

export default function Sidebar() {
  return (
    <div style={{ display: 'flex', flex: 0.1 }}>
      <div
        style={{
          width: '300px',
          backgroundColor: '#fff',
          padding: '3rem 0rem',
          borderRight: '1px solid #e2e2e3',
          height: '100%',
        }}
      >
        <div
          style={{
            backgroundColor: '#09b2f0',
            borderTopRightRadius: '50px',
            borderBottomRightRadius: '50px',
            marginRight: '1rem',
            display: 'flex',
            flexDirection: 'row',
          }}
        >
          <div
            style={{
              paddingLeft: '2rem',
              paddingTop: '0.3rem',
              paddingBottom: '0.2rem',
            }}
          >
            <AiOutlineAppstore
              style={{ color: 'white', height: '40px', width: '40px' }}
            />
          </div>
          <div
            style={{
              marginTop: '14px',
              marginLeft: '1rem',
              color: 'white',
            }}
          >
            <Link to='/MiddleSection' className='sidebarLink'>
              <p style={{ color: '#fff' }}>Dashboard</p>
            </Link>
            {/* <p>Dashboard</p> */}
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: '0 2rem',
            margin: '1rem 0',
          }}
        >
          <div
            style={{
              display: 'flex',
            }}
          >
            <BiCalendar
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
            <div
              style={{
                marginLeft: '1rem',
                color: '#858585',
                marginTop: '10px',
              }}
            >
              <Link to='/calendar'>Calendar</Link>
            </div>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: '0 2rem',
            margin: '1rem 0',
          }}
        >
          <div
            style={{
              display: 'flex',
            }}
          >
            <BsBriefcase
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
            <div
              style={{
                marginLeft: '1rem',
                color: '#858585',
                marginTop: '10px',
              }}
            >
              <Link to='/addCases' className='sidebarLink'>
                Cases
              </Link>
            </div>
          </div>
          <div>
            <IoIosArrowDropright
              size={30}
              color='#09b2f0'
              style={{ marginTop: '5px' }}
            />
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: '0 2rem',
            margin: '1rem 0',
          }}
        >
          <div
            style={{
              display: 'flex',
            }}
          >
            <HiOutlineDocumentText
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
            <div
              style={{
                marginLeft: '1rem',
                color: '#858585',
                marginTop: '10px',
              }}
            >
              <p>Document</p>
            </div>
          </div>
          <div>
            <IoIosArrowDropright
              size={30}
              color='#09b2f0'
              style={{ marginTop: '5px' }}
            />
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: '0 2rem',
            margin: '1rem 0',
          }}
        >
          <div
            style={{
              display: 'flex',
            }}
          >
            <HiOutlineTicket
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
            <div
              style={{
                marginLeft: '1rem',
                color: '#858585',
                marginTop: '10px',
              }}
            >
              <Link to='/order' className='sidebarLink'>
                Order
              </Link>
            </div>
          </div>
          <div>
            <IoIosArrowDropright
              size={30}
              color='#09b2f0'
              style={{ marginTop: '5px' }}
            />
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: '0 2rem',
            margin: '1rem 0',
          }}
        >
          <div
            style={{
              display: 'flex',
            }}
          >
            <AiOutlineSound
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
            <div
              style={{
                marginLeft: '1rem',
                color: '#858585',
                marginTop: '10px',
              }}
            >
              <p>Hearing</p>
            </div>
          </div>
          <div>
            <IoIosArrowDropright
              size={30}
              color='#09b2f0'
              style={{ marginTop: '5px' }}
            />
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: '0 2rem',
            margin: '1rem 0',
          }}
        >
          <div
            style={{
              display: 'flex',
            }}
          >
            <HiOutlineDocumentText
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
            <div
              style={{
                marginLeft: '1rem',
                color: '#858585',
                marginTop: '10px',
              }}
            >
              <p>Todo's</p>
            </div>
          </div>
          <div>
            <IoIosArrowDropright
              size={30}
              color='#09b2f0'
              style={{ marginTop: '5px' }}
            />
          </div>
        </div>
        {/* <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                padding: "0 2rem",
                margin: "1rem 0",
              }}
            >
              <div
                style={{
                  display: "flex",
                }}
              >
                <HiOutlineDocumentText
                  style={{ color: "#09b2f0", height: "40px", width: "40px" }}
                />
                <div
                  style={{
                    marginLeft: "1rem",
                    color: "#858585",
                    marginTop: "10px",
                  }}
                >
                  <p>Todo's</p>
                </div>
              </div>
            </div> */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: '0 2rem',
            margin: '1rem 0',
          }}
        >
          <div
            style={{
              display: 'flex',
            }}
          >
            <BiSave
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
            <div
              style={{
                marginLeft: '1rem',
                color: '#858585',
                marginTop: '10px',
              }}
            >
              <p>View Case History</p>
            </div>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: '0 2rem',
            margin: '1rem 0',
          }}
        >
          <div
            style={{
              display: 'flex',
            }}
          >
            <BiBarChartSquare
              style={{ color: '#09b2f0', height: '40px', width: '40px' }}
            />
            <div
              style={{
                marginLeft: '1rem',
                color: '#858585',
                marginTop: '10px',
              }}
            >
              <p>Ecourts</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
