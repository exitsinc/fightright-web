import React from "react";
import Pagination from "react-bootstrap/Pagination";
import Table from "../Table/Table";
import { BsBriefcase, BsThreeDots } from "react-icons/bs";
import Style from "../RunningCases/Style";
import "../Dummy/dummy.css";
import Tbody from "../Table/Tbody/Tbody";
import { HiOutlineSearch } from "react-icons/hi";
import CaseBox from "../CaseBox/CaseBox";

export default function MiddleSection() {
  return (
    <div className="container-fluid" style={{}}>
      <div className="row">
        <div className="col-lg-12">
          <div className="search">
            <HiOutlineSearch
              style={{
                color: "#5b63a9",
                height: "40px",
                width: "40px",
                top: 3,
                padding: "10px",
                position: "relative",
                left: 40,
              }}
            />
            <input
              type="text"
              placeholder="Search"
              style={{
                width: "95%",
                border: "1px solid #e2e2e3",
                backgroundColor: "#fff",
                borderRadius: 50,
                padding: "10px",
                paddingLeft: "3rem",
                color: "#a5b2c4",
              }}
            />
          </div>
        </div>
        <div className="col-lg-12">
          <div style={{display:'flex',}}>
          <CaseBox percent={25} title="TOTAL NUMBER OF CASE DIARY" type="Cases"  typeCount={12}/>
          <CaseBox percent={25} title="TOTAL NUMBER OF CASE DIARY" type="Cases"  typeCount={12}/>
          <CaseBox percent={25} title="TOTAL NUMBER OF CASE DIARY" type="Cases"  typeCount={12}/>
          </div>
        </div>
        <div className="col-lg-12" style={{ backgroundColor: "#f7f7f7" }}>
          <div
            style={{
              background: "#f7f7f7",
              padding: "1rem 2rem",
            }}
          >
            <h3 style={{ marginBottom: 10 }}>Running Cases</h3>
            <div
              style={{
                background: "#fff",
                padding: 15,
                border: ".2rem solid #ececec",
                borderRadius: "8px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <div>
                  <h6 style={{ color: "#808080", fontWeight: "bold" }}>
                    Running Cases(1)
                  </h6>
                </div>
                <div>
                  <BsThreeDots fontSize={24} fontWeight="bold" />
                </div>
              </div>
              <div>
                <div
                  className="row"
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    padding: "1.0rem 1.5rem",
                  }}
                >
                  <table class="table" className="borderless" id="RoundedTable">
                    <thead>
                      <tr>
                        <th
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Date
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Title
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                            flexDirection: "column",
                          }}
                        >
                          Appare Model
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Petitioner
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Case No.
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Priorty
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Category
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Before Judge
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Classification
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                            textAlign: "center",
                          }}
                        >
                          Status
                        </th>
                        <th
                          scope="col"
                          style={{
                            borderBottom: "none",
                            padding: "1rem",
                            fontSize: "16px",
                            color: "#858585",
                          }}
                        >
                          Action
                        </th>
                      </tr>
                    </thead>

                    <div style={{ marginTop: 20 }}></div>

                    <Table
                      date="15/02/2020"
                      title="Enter the title"
                      Petitioner=""
                      Petitioner2="Petitioner"
                      caseno="3"
                      Critical="Super Critical"
                      Select="Selection"
                      Judge="Before Hon`ble Judge"
                      Classification="Classification"
                      bgcolor="#5df888"
                      status="Completed"
                      // dot="..."
                    />
                    <div style={{ marginTop: 20 }}></div>

                    <Table
                      date="15/02/2020"
                      title="Enter the title"
                      Petitioner=""
                      Petitioner2="Petitioner"
                      caseno="3"
                      Critical="Super Critical"
                      Select="Selection"
                      Judge="Before Hon`ble Judge"
                      Classification="Classification"
                      bgcolor="#ec1616"
                      status="Pending"
                      // dot="..."
                    />
                    <div style={{ marginTop: 20 }}></div>

                    <Table
                      date="15/02/2020"
                      title="Enter the title"
                      Petitioner=""
                      Petitioner2="Petitioner"
                      caseno="3"
                      Critical="Super Critical"
                      Select="Selection"
                      Judge="Before Hon`ble Judge"
                      Classification="Classification"
                      bgcolor="#ec1616"
                      status="Pending"
                      // dot="..."
                    />
                    <div style={{ marginTop: 20 }}></div>

                    <Table
                      date="15/02/2020"
                      title="Enter the title"
                      Petitioner=""
                      Petitioner2="Petitioner"
                      caseno="3"
                      Critical="Super Critical"
                      Select="Selection"
                      Judge="Before Hon`ble Judge"
                      Classification="Classification"
                      bgcolor="#5df888"
                      status="Completed"
                      // dot="..."
                    />
                  </table>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "2rem",
                    }}
                  >
                    <button
                      type="button"
                      class="btn btn-primary"
                      style={{
                        padding: "0.8rem 5rem",
                        borderRadius: " 50px",
                        backgroundColor: "#09b2f0",
                      }}
                    >
                      View More
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
