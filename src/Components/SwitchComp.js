import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Order from './Order/order'
import MiddleSection from './MiddleSection/MiddleSection'
import AddCases from './Cases/AddCases/addCases'
import Calendar from '../Components/Calendar/DemoApp'

const SwitchComp = () => {
  return (
    <Switch>
      <Route path='/calendar'>
        <Calendar />
      </Route>
      <Route path='/addCases'>
        <AddCases />
      </Route>
      <Route path='/order'>
        <Order />
      </Route>
      <Route path='/'>
        <MiddleSection />
      </Route>
    </Switch>
  )
}
export default SwitchComp
