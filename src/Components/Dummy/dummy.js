import React from "react";
import "./dummy.css";
import { GrNotification } from "react-icons/gr";
import { CgProfile } from "react-icons/cg";
import { AiOutlineAppstore, AiOutlineSound } from "react-icons/ai";
import { BiCalendar, BiSave, BiBarChartSquare } from "react-icons/bi";
import { IoIosArrowDropright } from "react-icons/io";
import { HiOutlineDocumentText, HiOutlineTicket } from "react-icons/hi";
import { BsBriefcase, BsThreeDots } from "react-icons/bs";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import { BsDownload } from "react-icons/bs";
import { FiFilter } from "react-icons/fi";
import { HiOutlineSearch } from "react-icons/hi";
import Pagination from "react-bootstrap/Pagination";
import Table from "../Table/Table";
import Navbar from "../navbar/Navbar";
import Sidebar from "../Sidebar/Sidebar";
import MiddleSection from "../MiddleSection/MiddleSection";

const GREY = "#9E9E9E";
const percentage = 66;
export default function dummy() {
  return (
    <div style={{ height: "100vh" }}>
      <Navbar />
      <div style={{ display: "flex" }}>
        <Sidebar />
        <div
          style={{
            display: "flex",
            flex: 0.9,
            padding: "0rem 0rem",
            flexDirection: "column",
            backgroundColor: "#f9f9f9",
          }}
        >
         <MiddleSection />
        </div>
      </div>
    </div>
  );
}
