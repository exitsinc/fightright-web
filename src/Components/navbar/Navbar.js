import React from "react";
import { GrNotification } from "react-icons/gr";
import { CgProfile } from "react-icons/cg";
import profile from "./profile-pic.jpg";
import "./Navbar.css";

export default function Navbar() {
  return (
    <div
      style={{
        height: "10vh",
        backgroundColor: "#fff",
        borderBottom: "1px solid #e2e2e3",
      }}
    >
      <div className="container-fluid">
        <div className="row">
          <div
            className="header"
            style={{
              display: "flex",
              justifyContent: "space-between",
              flexDirection: "row",
            }}
          >
            <div>
              <img className="header-logo" src="img/logo.png" />
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                flexDirection: "row",
              }}
            >
              <div style={{ padding: "0 2rem" }}>
                <GrNotification size={32} style={{position: "relative",top: "10px"}} />
              </div>
              <div
                style={{
                  padding: "0 2rem",
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                <img
                  src={profile}
                  alt="image not found"
                  style={{ width: "50px", height: "50px", borderRadius: "50%" }}
                ></img>
                {/* <p style={{ margin: "0 1rem" }}>ANDREW</p> */}
                <div class="dropdown">
                  <button
                    class="dropbtn"
                    style={{ display: "flex", justifyContent: "space-around" }}
                  >
                    ANDREW
                  </button>
                  <p
                    style={{
                      color:"#858585",
                      position: " relative",
                      paddingLeft: "16px",
                      top: -17,
                    }}
                  >
                    Admin account
                  </p>
                  <div class="dropdown-content">
                    <a href="#">Profile</a>
                    <a href="#">Settings</a>
                    <a href="#">Logout</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
