import React from "react";
import { BsThreeDots } from "react-icons/bs";
import "../Dummy/dummy.css";

function Style({
  date,
  title,
  appareModel,
  petitioner,
  caseNo,
  priority,
  category,
  beforeJudge,
  classification,
  status,
  bgcolor,
}) {
  console.log(
    date,
    title,
    appareModel,
    petitioner,
    caseNo,
    priority,
    category,
    beforeJudge,
    classification,
    status
  );
  return (
    <>
      <tr className="tr">
        <td>{date}</td>
        <td style={{ color: "#858585", fontWeight: "bold" }}>{title}</td>
        <td style={{color:'#858585'}}>{appareModel}</td>
        <td style={{color:'#858585'}}>{petitioner}</td>
        <td style={{color:'#858585'}}>{caseNo}</td>
        <td style={{color:'#858585'}}>{priority}</td>
        <td style={{color:'#858585'}}>{category}</td>
        <td style={{color:'#858585'}}>{beforeJudge}</td>
        <td style={{color:'#858585'}}>{classification}</td>
        <td style={{color:'#858585'}}>
          <button
            type="button"
            className={`btn btn-${bgcolor}`}
            style={{
              backgroundColor: bgcolor,
              color: "#fff",
              fontWeight: "bold",
              padding: "0.1rem 1.5rem",
              fontSize: "14px",
              borderRadius:'50px'
            }}
          >
            {status}
          </button>
        </td>
        <td>
          <BsThreeDots
            style={{
              color: "#858585",
              height: "25px",
              width: "25px",
            }}
          />
        </td>
      </tr>
    </>
  );
}

export default Style;
