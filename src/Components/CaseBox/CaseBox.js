import React from 'react'
import '../CaseBox/CaseBox.css'
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { BsThreeDots } from 'react-icons/bs';
export default function CaseBox(props) {
    return (
      <>
        <div className="col-md-4" style={{ backgroundColor: "" }}>
            <div className="outerCaseBox">
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  paddingRight: "1rem",
                }}
              >
                <span>
                  <BsThreeDots size={30} color="#858585" />
                </span>
              </div>
              <div className="caseBox">
                <div>
                  <div style={{ width: 100, height: 100 }}>
                    <CircularProgressbar
                      value={props.percent}
                      text={`${props.percent}%`}
                    />
                  </div>
                </div>
                <div style={{padding:'0rem 2rem'}}>
                  <p style={{ fontSize: 16,fontWeight:"bold",color:'#858585',width:"80%",marginBottom:0}}>
                    {props.title}
                  </p>
                  <p style={{ fontSize: 14 }}>
                   Pending {props.type} : <span style={{ fontSize: 18,fontWeight:'bold' }}>{props.typeCount}</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
      </>
    )
}
