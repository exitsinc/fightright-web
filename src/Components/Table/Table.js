import React from "react";
import "../Table/Table.css";
import { BsThreeDots } from "react-icons/bs";

export default function Table(props) {
  const {
    date,
    title,
    Petitioner,
    Petitioner2,
    caseno,
    Critical,
    Select,
    Judge,
    Classification,
    bgcolor,
    status,
  } = props;

  console.log(props);
  return (
    <>
      <tbody className="cross">
        <tr>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
            }}
          >
            {date}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
              fontWeight: "700",
            }}
          >
            {title}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
            }}
          >
            {Petitioner}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
            }}
          >
            {Petitioner2}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
            }}
          >
            {caseno}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
            }}
          >
            {Critical}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
            }}
          >
            {Select}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
              fontWeight: 500,
            }}
          >
            {Judge}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
            }}
          >
            {Classification}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
            }}
          >
            <button
              type="button"
              className={`btn btn-${bgcolor}`}
              style={{
                borderRadius: 20,
                color: "#fff",
                fontWeight: "bold",
                backgroundColor: bgcolor,
                padding: "0.3rem 1rem",
                width: 120,
              }}
            >
              {status}
            </button>
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
            }}
          >
            <BsThreeDots size={25} style={{color:"#858585"}} />
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem",
              borderTopRightRadius: 10,
              borderBottomRightRadius: 10,
            }}
          ></td>
        </tr>
      </tbody>
    </>
  );
}
