import React from "react";
import { BsThreeDots } from "react-icons/bs";

export default function Tbody(props) {
  const { fromdate, todate, Description, Related, Pending, bgcolor } = props;
  return (
    <>
      <tbody className="cross">
        <tr>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem 2rem ",
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
            }}
          >
            {fromdate}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem 2rem ",
              fontWeight: "700",
            }}
          >
            {todate}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem 5rem ",
            }}
          >
            {Description}
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              paddingLeft: "3rem",
            }}
          >
            {Related}
          </td>

          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem 3.7rem ",
            }}
          >
            <button
              type="button"
              className={`btn btn-${bgcolor}`}
              style={{
                borderRadius: 20,
                color: "#fff",
                fontWeight: "bold",
                backgroundColor: bgcolor,
                padding: "0.3rem 1rem",
                width: 120,
              }}
            >
              {Pending}
            </button>
          </td>
          <td
            style={{
              color: "#808080",
              fontSize: "12px",
              padding: "1rem 3rem",
              borderTopRightRadius: 10,
              borderBottomRightRadius: 10,
            }}
          >
            <BsThreeDots size={25} style={{color:"#858585"}} />
          </td>
        </tr>
      </tbody>
    </>
  );
}
