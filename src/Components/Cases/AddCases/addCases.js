import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { IoDownloadOutline, IoEllipsisHorizontal } from 'react-icons/io5'
import { RiCalendar2Fill } from 'react-icons/ri'
import DatePicker from 'react-datepicker'
import './addCases.css'
const AddCases = () => {
  return (
    <React.Fragment>
      <div className='cont'>
        <div className='header'>
          <h2>Add Cases</h2>
        </div>
        <div className='icons'>
          <IoDownloadOutline
            style={{
              fontSize: 40,
              cursor: 'pointer',
              color: '#858585',
              marginRight: 20,
            }}
          />
          <IoEllipsisHorizontal
            style={{ fontSize: 40, cursor: 'pointer', color: '#858585' }}
          />
        </div>
      </div>

      <div className='card'>
        <div className='card-body'>
          <div className='row'>
            <div className='col-md-6'>
              <label className='form-label'>Court</label>
              <input
                type='text'
                className='form-control'
                placeholder='Select'
              />
            </div>
            <div className='col-md-6'>
              <label for='case#' className='form-label'>
                Case Number
              </label>
              <input
                id='case#'
                type='text'
                className='form-control'
                placeholder='Case Number'
              />
            </div>
          </div>
          <div className='row'>
            <div className='col-md-6'>
              <label>Select State</label>
              <select class='form-control'>
                <option selected>Select State</option>
                <option>...</option>
              </select>
            </div>
            <div className='col-md-6'>
              <label>Select Year</label>
              <select class='form-control'>
                <option selected>Select Year</option>
                <option>...</option>
              </select>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-6'>
              <label className='form-label'>Petitioner #</label>
              <input
                type='text'
                className='form-control'
                placeholder='Petitioner'
              />
            </div>
            <div className='col-md-6'>
              <label className='form-label'>Date of filling</label>
              <input type='date' className='form-control' />
            </div>
          </div>
          <div className='row'>
            <div className='col-md-6'>
              {' '}
              <label className='form-label'>Court Hall #</label>
              <input
                type='text'
                className='form-control'
                placeholder='Select'
              />
            </div>
            <div className='col-md-6'>
              <label className='form-label'>Floor #</label>
              <input
                type='text'
                className='form-control'
                placeholder='Select'
              />
            </div>
          </div>
          <div className='row'>
            <div className='col-md-6'>
              <label className='form-label'>Classification</label>
              <input
                type='text'
                className='form-control'
                placeholder='Select'
              />
            </div>
            <div className='col-md-6'>
              <label className='form-label'>Title</label>
              <input
                type='text'
                className='form-control'
                placeholder='Select'
              />
            </div>
          </div>
          <div className='row'>
            <div className='col-md-12'>
              <button type='button' className='button '>
                Submit
              </button>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default AddCases
